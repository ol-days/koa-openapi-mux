import _ from "lodash"
import oaiparser from "swagger-parser"
import compose from "koa-compose"
import autobind from "auto-bind"
import WaitGroup from "wait-group-promise"

export {Mux}
export {Mux as default}

class Mux {
  //whenoai = null //{openapi specification}
  //waitgroup //waitgroup to make sure all configuration functions are done before we start serving endpoints
  //handlers = {operationId: [fn]}

  constructor(conf) {
    if (!_.isObject(conf.handlers)) {
      throw new Error("handlers must be object")
    }

    //Properties
    this.waitgroup = new WaitGroup
    this.whenoai = oaiparser.dereference(conf.api).then(Object.freeze)
    this.handlers = _.mapValues(conf.handlers, fn => [fn])

    //have to add this because waitgroup errors if there is not at least 1 task
    let wg = this.waitgroup
    wg.add()
    this.whenoai.then(() => wg.done())

    autobind(this)
  }

  use(selector, middleware) {
    let wg = this.waitgroup
    try {
      wg.add()
    } catch (e) {
      console.log(e)
      throw new Error("You must do all Router.use() calls before using Router.middleware")
    }

    this.whenoai.then((oai) => {
      //TODO add middleware to appropriate this.handlers
      wg.done()
    })
  }

  //wrap in function because we want to force waiting on promises
  get middleware() {
    let readyToServe = this.waitgroup.wait()

    return (async function dispatch(ctx, next) {
      await readyToServe
      let oai = await this.whenoai //this should be immediately ready at this point

      let opId = this.getOpIdForRequest(ctx.request, oai)
      if (opId === undefined) {
        //console.log("no operationId defined for: " + ctx.request.path)
        return await next()
      }
      let op = this.handlers[opId]
      if (!op) {
        //console.log("no operation handler for: " + opId)
        return await next()
      }
      //call the middlewares and handler for the resolved path
      //Dont pass next to this because we dont want downstream middleware to do anything
      return await compose(op)(ctx)

    }).bind(this) //has to be manually bound because its scoped
  }


  //===================
  //Internal Utility Functions
  //===================

  //TODO: consider implementing this ahead of time. https://github.com/OAI/OpenAPI-Specification/issues/1459
  getOpIdForRequest(request, oai) {
    let method = _.toLower(request.method)

    let path = _(oai.paths)
      .keys()
      .filter(path => pathMatches(path, request.path))
      .reduce(betterMatch) //reduce returns value not wrapper

    if (path == undefined || !oai.paths[path][method]) {
      return undefined
    } else {
      return oai.paths[path][method].operationId
    }
  }
}

//it is assumed that path1 and path2 have the same number of  path segments
function betterMatch(path1, path2) {
  let path1Segs = path1.split("/")
  let path2Segs = path2.split("/")

  for (let i=0; i < path1Segs.length; i++) {
    if (path1Segs[i][0] !== "{" && path2Segs[i][0] === "{") {
      return path1
    }
    if (path1Segs[i][0] === "{" && path2Segs[i][0] !== "{") {
      return path2
    }
  }
}

function pathMatches(path, test) {
  let pathSegs = path.split("/")
  let testSegs = test.split("/")
  if (pathSegs.length !== testSegs.length) {
    return false
  }

  for (let i=0; i < pathSegs.length; i++) {
    if (pathSegs[i][0] === "{"){
      continue
    } else if (pathSegs[i] !== testSegs[i]) {
      return false
    }
  }
  return true
}

// const configExample = {
//   //required
//   api: {/*OpenAPI specification*/} || "path/to/api/file",
//   //required: handlers for operations
//   handlers: {
//     getPet: pets.get
//     ...
//   },
//   //optional
//   executor: function(ctx, next, handler) {
//     handler.apply(ctx, ctx.whatever)
//   }
// }
