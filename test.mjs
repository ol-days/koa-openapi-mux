import Router from "./index.mjs"
import Koa from "koa"

const server = new Koa()

let oairouter = new Router({api: "petstore.yaml", handlers:{
  listPets: async (ctx, next) => {
    console.log("listing pets!")
    await next()
  }
}})

oairouter.use()
oairouter.use()
server.use(oairouter.middleware)

server.use(async (ctx, next)=>{
  console.log("endchain")
  await next()
})

server.listen(8080)
